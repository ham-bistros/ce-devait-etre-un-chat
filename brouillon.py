#!/usr/bin/env python3

import os
import random

artistes = ['myr_muratet', 'rodrigue_costello', 'dominique_blais']

for artiste in artistes:
    if os.path.isdir('content/%s' % artiste):
        print(os.listdir('content/%s' % artiste))
