#Loïs Renou & Anouk Jeanningros, "Et si ?", 16/12/2021.
#Copyleft with a difference: This is a collective work, you are invited to copy, distribute, and modify it under the terms of the CC4r.
#[https://constantvzw.org/wefts/cc4r.en.html]

#import the modules needed for this code
import random
# import time

#----------------------------------------------#
#------------------ VARS ----------------------#
#----------------------------------------------#

#read various texts files
#noms le/la
source = open('ressources/noms_le_la.txt', 'r' , encoding = 'utf-8')
noms_le_la = source.read()
source.close()
#transform the text into list of string
noms_le_la_split = noms_le_la.split("\n")
# print(noms_le_la_split)

#noms le
source = open('ressources/noms_le_m.txt', 'r' , encoding = 'utf-8')
noms_le_m = source.read()
source.close()

noms_le_m_split = noms_le_m.split("\n")

#noms la
source = open('ressources/noms_la_f.txt', 'r' , encoding = 'utf-8')
noms_la_f = source.read()
source.close()

noms_la_f_split = noms_la_f.split("\n")

#noms un/une
source = open('ressources/noms_un_une.txt', 'r' , encoding = 'utf-8')
noms_un_une = source.read()
source.close()

noms_un_une_split = noms_un_une.split("\n")

#adj masculin
source = open('ressources/adj_m.txt', 'r' , encoding = 'utf-8')
adj_m = source.read()
source.close()

adj_m_split = adj_m.split("\n")

#adj feminin
source = open('ressources/adj_f.txt', 'r' , encoding = 'utf-8')
adj_f = source.read()
source.close()

adj_f_split = adj_f.split("\n")

#verbes imparfait
source = open('ressources/verbes_imparfait.txt', 'r' , encoding = 'utf-8')
verbes_imparfait = source.read()
source.close()

verbes_imparfait_split = verbes_imparfait.split("\n")

#create a dictionary ("Alex Didier" is a key and ["sonorité", "instrument",(...)] are the values of the key)
artiste=  {"Alex Didier":["sonorité", "l'instrument", "instrument", "larsen", "écosystème", "l'écosystème", "loop",	"poétique", "connecté", "connectée", "sonore", "immersif", "immersive", "l'installation", "installation", "dispositif", "l'expérience", "expérience"],
"Alexandre Roux":["carrosserie", "véhicule", "graffiti", "détail", "irréel", "irréelle", "tridimensionnel", "tridimensionnelle", "technique", "montage", "l'image", "image", "textile","prototype"],
"Anas Kaaouachi":["souvenir", "témoignage", "journal", "peste", "imprimé", "imprimée", "froissé", "froissée", "photographié", "photographiée", "instable", "aléatoire", "discontinu", "discontinue", "biographique", "simulation", "photographie"],
"Aziza Daou":["récit", "reste", "absence", "l'absence", "médiocrité", "liberté", "fantasmé", "fantasmée", "protéiforme", "évolutif", "évolutive", "réel", "réelle", "performance", "texte"],
"Chloé Elvezi":["signe", "typographie", "forme","l'artefact", "artefact", "symbole", "l'archéologie", "archéologie", "mot", "fragment", "enfoui", "enfouie", "déterré", "déterrée", "transformé", "transformée", "l'alphabet", "alphabet", "système"],
"Erine Suzin": ["couleur", "végétal", "gestuelle", "sensation", "pâte", "modeler", "modelé", "modelée", "répété", "répétée", "machinal", "machinale", "digital", "digitale", "peinture"],
"Lucas Trotereau": ["bivouac", "QR", "gant", "désert", "motif", "camouflage", "extérieur", "extérieure", "modélisation","l'exposition", "exposition"],
"Marick Roy": ["drapé", "matière", "l'objet", "objet", "nombre", "matériel", "matérielle", "artificiel", "artificielle", "hétéroclite", "composition","vidéo"],
"Marie Ballay": ["quotidien", "parole", "capture", "écran", "période", "l'intimité", "intimité", "abandonné", "abandonnée", "photographique", "vidéo", "l'autoportrait", "autoportrait"],
"Marion Bouvret": ["réminiscence", "vestige", "bribe", "fragmenté", "fragmentée", "immobile","introspectif", "introspective", "prose"],
"Maureen Leprêtre": ["normographe", "glyphe", "l'archive", "archive", "l'illustration", "illustration", "puzzle", "inclusif", "inclusive", "engagé", "engagée", "politique", "graphisme"],
"Mischa Sanders": ["moulage", "béton", "croquis", "nombre", "processus", "travail", "colonne", "manuel", "manuelle", "physique", "sculpture"],
"Myrtille Chevalier": ["scan", "champignon", "ego", "l'édition", "édition", "viscosité", "collaboratif", "collaborative", "poésie", "folio"],
"Pauline Faivre": ["page", "biche", "forêt", "accident", "l'accident", "métamorphose", "l'histoire", "histoire"],
"Poliana Khoroudji": ["format", "dessin", "abstrait", "numérique", "l'esquisse", "esquisse", "mystérieux", "mystérieuse"],
"Rodrigue Costello": ["additive", "mouvement", "new", "glitch", "rétro", "synesthésique"],
"Solène Collin":["vide", "public", "regard", "l'infrastructure", "infrastructure", "métallique", "industriel", "industrielle", "l'assemblage", "assemblage", "manifeste"],
"Yuqi Zhang": ["montagne", "transformation", "nostalgie", "solitude", "terre", "boîte", "bois", "blanc", "blanche", "conte", "xylographie"],
"Audrey Templier":["pendentif", "bijou", "métal", "céramique", "rencontre", "curiosité", "recherche", "combinaison", "femme", "géométrique"],
"Myr Muratet": ["film", "bichromie", "filtre", "portrait", "paysage", "morte", "laboratoire", "chimie", "alternatif", "alternative", "affranchie", "affranchi", "altérée", "alteré"],
"Rudy Guedj":["cinéma", "fiction", "narration", "livre", "projection", "documentation", "historique", "architectural", "architecturale", "fictif", "fictive"],
"Dominique Blais": ["fantôme", "parabole", "miroir", "illusion", "l'illusion", "bibliothèque", "marbrure", "garde", "voix", "pièce", "transposé", "transposée", "silencieux", "silencieuse"],
"Élisa Pône" : ["résidence", "requin", "figure", "crainte", "désir", "pulsion", "incarnation", "visible", "cristallisé", "cristallisée", "intangible", "incarnation", "intention"], }


#----------------------------------------------#
#------------------ CODE ----------------------#
#----------------------------------------------#

def run():
#TO AGREE (noun+adj for ex)
#choose a name at random from the list
    substantif_le_la = random.choice(noms_le_la_split)
#IF the name chosen in the list is also in the 'noms masculin' list THEN choose an adjective at random from the list 'adj masculin'
    if substantif_le_la in noms_le_m_split:
        adjectif = random.choice(adj_m_split)

#ELSE feminin 'nom' selected then choose an adjective at random from the list 'adj féminin'
    else:
        adjectif = random.choice(adj_f_split)

    substantif_un_une = random.choice(noms_un_une_split)

#sentence constructed with a noun starting with 'le' or 'la', an adjective, a verb and a subtantive starting with 'un' or 'une'
    sentence= 'Et si' + ' ' + substantif_le_la + ' ' + adjectif + ' ' + random.choice(verbes_imparfait_split) + ' ' + substantif_un_une + ' ' + '?''\n'

#find out which artists the words come from :)
#transform the sentence into string list
    sans=sentence.replace("l'", "")
    sentencesplit= sans.split(" ")


#for each word in the string list 'sentence split'
    # call_artiste=""
    call_artiste = []
    for x in sentencesplit:
        #for each keys, values in the dictionary
        for name, linked_words in artiste.items():
            if x in linked_words:
                name = name.lower().replace(' ', '_')
                call_artiste.append(name)

    call_artiste = list(dict.fromkeys(call_artiste))

    return sentence, call_artiste
