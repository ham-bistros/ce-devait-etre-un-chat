const cursorBase = document.getElementById('cursor-base');
const cursorLiens = document.getElementById('cursor-liens');

const moveCursor = (e)=> {
  const mouseY = e.clientY;
  const mouseX = e.clientX;

  cursorBase.style.transform = `translate3d(${mouseX}px, ${mouseY}px, 0)`;
  cursorLiens.style.transform = `translate3d(${mouseX}px, ${mouseY}px, 0)`;

}

window.addEventListener('mousemove', moveCursor);

[...document.getElementsByTagName('a')].forEach(function(el) {
  el.addEventListener('mouseenter', function(el) {
    cursorBase.style.display = 'none';
    cursorLiens.style.display = 'block';

    this.addEventListener('mouseout', function() {
      cursorBase.style.display = 'block';
      cursorLiens.style.display = 'none';
    });

  });
})

