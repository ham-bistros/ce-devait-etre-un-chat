function randInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();

    main.appendChild(e.target.parentElement);

    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

const main = document.getElementsByTagName("main")[0]
const content = document.getElementsByClassName('content');

var windowWidth = document.body.clientWidth;
var windowHeight = document.body.clientHeight;

setTimeout(function() {
  [...content].forEach(function (el) {
    el.style.display = 'block';

    maxLeft = randInteger(50, windowWidth * 0.8) + 'px';
    maxTop = randInteger(50, windowHeight * 0.8) + 'px';

    el.style.left = maxLeft;
    el.style.top = maxTop;

    // console.log(el);
    // console.log(maxLeft);
    // console.log(maxTop);

    // Make the element draggable:
    dragElement(el);
  });
}, 5000);
