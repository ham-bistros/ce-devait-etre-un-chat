#!/usr/bin/env python3

import bottle
import random
import os
import csv

import etsi

def readCSV(file):
    with open(file) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        csvdata = list(spamreader)
        csvdata.pop(0)
        return csvdata

# CÉPARTI
@bottle.get('/favicon.ico')
def get_favicon():
    return bottle.static_file('favicon.ico', root="./")

@bottle.get("/static/<filepath:path>")
def static(filepath):
    return bottle.static_file(filepath, root="static/")

@bottle.get("/content/<filepath:path>")
def content(filepath):
    return bottle.static_file(filepath, root="content/")

@bottle.route('/')
def home ():
    print('coucou')
    return bottle.template('index.html')

@bottle.route('/contact')
def contact() :
    return bottle.template('static/templates/contact.html')

@bottle.route('/artiste')
@bottle.route('/artiste/<name>')
def artiste(name='') :
    artistes = readCSV('static/adresse_artistes_site_web.csv')

    boulots = {
        "imgs": [],
        "txts": []
    }

    if name:
        for boul in os.listdir('content/%s' % name):
            if boul[-4:-1] == '.pd':
                boulots["txts"].append('../content/%s/%s' % (name, boul))
            else:
                boulots["imgs"].append('../content/%s/%s' % (name, boul))

    return bottle.template('static/templates/artiste.html', artistes=artistes, name=name, boulots=boulots)

@bottle.route('/etsi')
def alors() :
    phrase, artistes = etsi.run()

    contenu = []
    for artiste in artistes:
        image = random.choice(os.listdir('content/%s' % artiste))
        contenu.append((artiste, image))

    return bottle.template('static/templates/etsi.html', phrase=phrase, contenu=contenu)

bottle.run(host='0.0.0.0', port=5000, debug=True, reload=True)

